from django.shortcuts import render
from django.views.generic import View
from django.contrib import messages

from core.forms import ResumeForm
from core.models import Resume

# Create your views here.

class Home(View):
    def get(self, request):
        form = ResumeForm()
        candidates = Resume.objects.all()
        context = {
            'form': form,
            'candidates': candidates
        }
        return render(request, 'core/home.html', context)
    

    def post(self, request):
        form = ResumeForm(data=request.POST, files=request.FILES)
        candidates = Resume.objects.all()
        if form.is_valid():
            form.save()
            messages.success(request, 'Your resume is submitted!!')
            form = ResumeForm()
        context = {
            'form': form,
            'candidates': candidates
        }
        return render(request, 'core/home.html', context)


class CandidateDetail(View):
    def get(self, request, pk):
        candidate = Resume.objects.get(id=pk)
        context = {
            'candidate': candidate
        }
        return render(request, 'core/candidate.html', context)
