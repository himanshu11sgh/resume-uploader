from django.db import models


STATE_CHOICES = (
    ('Andaman & Nicobar Islands', 'Andaman & Nicobar Islands'),
    ('Andhra Pradesh', 'Andhra Pradesh'),
    ('Aruncachal Pradesh', 'Aruncachal Pradesh'),
    ('Assam', 'Aruncachal Pradesh'),
    ('Bihar', 'Bihar'),
    ('Chandigarh', 'Chandigarh'),
    ('Chhattisgarh', 'Chhattisgarh'),
    ('Dadra & Nagar Haveli', 'Dadra & Nagar Haveli'),
    ('Daman and Diu', 'Daman and Diu'),
    ('Delhi', 'Delhi'),
    ('Goa', 'Goa'),
    ('Gujarat', 'Gujarat'),
    ('Haryana', 'Gujarat'),
    ('Himachal Pradesh', 'Himachal Pradesh'),
    ('Jammu & Kashmir', 'Jammu & Kashmir'),
    ('Jharkhand', 'Jharkhand'),
    ('Karnataka', 'Karnataka'),
    ('Kerala', 'Kerala'),
    ('Lakshadweep', 'Lakshadweep'),
    ('Madhya Pradesh', 'Madhya Pradesh'),
    ('Maharashtra', 'Maharashtra'),
    ('Manipur', 'Manipur'),
    ('Meghalaya', 'Meghalaya'),
    ('Mizoram', 'Mizoram'),
    ('Nagaland', 'Nagaland'),
    ('Odisha', 'Odisha'),
    ('Puducherry', 'Puducherry'),
    ('Punjab', 'Punjab'),
    ('Rajasthan', 'Rajasthan'),
    ('Sikkim', 'Sikkim'),
    ('Tamil Nadu', 'Tamil Nadu'),
    ('Telangana', 'Telangana'),
    ('Tripura', 'Tripura'),
    ('Uttrakhand', 'Uttrakhand'),
    ('Uttar Pradesh', 'Uttar Pradesh'),
    ('West Bengal', 'West Bengal'),
)


class Resume(models.Model):
    name = models.CharField(max_length=255)
    dob = models.DateField(auto_now_add=False, auto_now=False)
    gender = models.CharField(max_length=255)
    email = models.EmailField()
    locality = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    pin = models.PositiveIntegerField()
    state = models.CharField(choices= STATE_CHOICES, max_length=255)
    mobile = models.PositiveIntegerField()
    job_city = models.CharField(max_length=255)
    profile_img = models.ImageField(upload_to='images/profile', blank=True)
    my_file = models.FileField(upload_to='documents/profile', blank=True)

    