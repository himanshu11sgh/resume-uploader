from django.contrib import admin

from core.models import Resume

@admin.register(Resume)
class ResumeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'dob', 'gender', 'email', 'locality', 'city', 'pin', 'state', 'mobile', 'job_city', 'profile_img', 'my_file' )
    