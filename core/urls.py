from django.urls import path

from core.views import CandidateDetail, Home


urlpatterns = [ 
    path('', Home.as_view(), name='home'),
    path('<int:pk>/', CandidateDetail.as_view(), name='candidate')
]